using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Unity.Scenes;
using Unity.Scenes.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using ListView = Unity.Editor.Bridge.ListView;
using Object = UnityEngine.Object;

namespace Unity.Entities.Editor.Tests
{
    [TestFixture]
    [SuppressMessage("ReSharper", "Unity.InefficientPropertyAccess")]
    class EntityWindowIntegrationTests : EntityWindowIntegrationTestFixture
    {
        protected override string SceneName { get; } = nameof(EntityWindowIntegrationTests);

        [UnityTest]
        public IEnumerator BasicSubsceneBasedParenting_ProducesExpectedResult()
        {
            // Adding a GameObject to a SubScene
            var go = new GameObject("go");
            SceneManager.MoveGameObjectToScene(go, SubScene.EditingScene);

            yield return UpdateLiveLink();

            var (expectedHierarchy, subScene, _, entityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, entityId, 0)); // "go" Converted Entity

            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            // Asserts that the names of the scenes were correctly found.
            using (var rootChildren = PooledList<EntityHierarchyNodeId>.Make())
            {
                Window.State.GetChildren(EntityHierarchyNodeId.Root, rootChildren.List);

                var sceneNode = rootChildren.List.Single(child => child.Kind == NodeKind.Scene);
                Assert.That(Window.State.GetNodeName(sceneNode), Is.EqualTo(SceneName));

                using (var sceneChildren = PooledList<EntityHierarchyNodeId>.Make())
                {
                    Window.State.GetChildren(sceneNode, sceneChildren.List);

                    // Only Expecting a single child here
                    var subsceneNode = sceneChildren.List[0];
                    Assert.That(Window.State.GetNodeName(subsceneNode), Is.EqualTo(SubSceneName));
                }

            }
        }

        // Scenario:
        // - Add GameObject to root of subscene
        // - Add second GameObject to root of subscene
        // - Parent second GameObject to first
        // - Unparent second GameObject from first
        // - Delete second GameObject
        [UnityTest]
        public IEnumerator SubsceneBasedParenting_Scenario1()
        {
            // Adding a GameObject to a SubScene
            var go = new GameObject("go");
            SceneManager.MoveGameObjectToScene(go, SubScene.EditingScene);
            yield return UpdateLiveLink();

            // Adding a second GameObject to a SubScene
            var go2 = new GameObject("go2");
            SceneManager.MoveGameObjectToScene(go2, SubScene.EditingScene);
            yield return UpdateLiveLink();

            var (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChildren(
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0),// "go" Converted Entity
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0)); // "go2" Converted Entity
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            // Parent second GameObject to first
            go2.transform.parent = go.transform;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0))    // "go" Converted Entity
                        .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0)); // "go2" Converted Entity
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            // Unparent second GameObject from first
            go2.transform.parent = null;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChildren(
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0),// "go" Converted Entity
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0)); // "go2" Converted Entity
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            // Delete second GameObject
            Object.DestroyImmediate(go2);
            yield return UpdateLiveLink();

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0)); // "go" Converted Entity

            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());
        }

        // Scenario:
        // 3 GameObjects [A, B, C] at the Root of a SubScene
        // Move A into B
        // Move A into C
        // Move B into C
        // Move A back to Root
        // Move B back to Root
        [UnityTest]
        public IEnumerator SubsceneBasedParenting_Scenario2()
        {
            var a = new GameObject("A");
            SceneManager.MoveGameObjectToScene(a, SubScene.EditingScene);
            yield return UpdateLiveLink();

            var b = new GameObject("B");
            SceneManager.MoveGameObjectToScene(b, SubScene.EditingScene);
            yield return UpdateLiveLink();

            var c = new GameObject("C");
            SceneManager.MoveGameObjectToScene(c, SubScene.EditingScene);
            yield return UpdateLiveLink();

            var (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChildren(
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0),
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0),
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            a.transform.parent = b.transform;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene
               .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0))
                    .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0));
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            a.transform.parent = c.transform;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0));
            subScene
               .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0))
                    .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            b.transform.parent = c.transform;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0))
                    .AddChildren(
                         new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0),
                         new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            a.transform.parent = null;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++, 0));
            subScene
               .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0))
                    .AddChild(new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());

            b.transform.parent = null;
            yield return UpdateLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility(); // Ensuring that all parenting phases have completed

            (expectedHierarchy, subScene, _, nextEntityId) = CreateBaseHierarchyForSubscene();
            subScene.AddChildren(
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0),
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId++ ,0),
                new EntityHierarchyNodeId(NodeKind.Entity, nextEntityId, 0));
            AssertHelper.AssertHierarchyByKind(expectedHierarchy.Build());
        }

        [UnityTest]
        public IEnumerator Search_ToggleSearchView()
        {
            // Adding a GameObject to a SubScene
            var go = new GameObject("go");
            var other = new GameObject("other");
            SceneManager.MoveGameObjectToScene(go, SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(other, SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("go");

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));

            SearchElement.Search(string.Empty);

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
        }

        [UnityTest]
        public IEnumerator Search_DisableSearchWhenSearchElementIsHidden()
        {
            // Adding a GameObject to a SubScene
            var go = new GameObject("go");
            var other = new GameObject("other");
            SceneManager.MoveGameObjectToScene(go, SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(other, SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("go");

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(SearchView.itemsSource
                            .Cast<EntityHierarchyItem>()
                            .Select(x => x.CachedName), Is.EquivalentTo(new[] { SubSceneName, "go" }));

            var searchIcon = WindowRoot.Q(className: UssClasses.DotsEditorCommon.SearchIcon);
            Window.SendEvent(new Event
            {
                type = EventType.MouseUp,
                mousePosition = searchIcon.worldBound.position
            });

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));

            Window.SendEvent(new Event
            {
                type = EventType.MouseUp,
                mousePosition = searchIcon.worldBound.position
            });

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(SearchView.itemsSource
                            .Cast<EntityHierarchyItem>()
                            .Select(x => x.CachedName), Is.EquivalentTo(new[] { SubSceneName, "go" }));
        }

        [UnityTest]
        public IEnumerator Search_NameSearch()
        {
            // Adding a GameObject to a SubScene
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(new GameObject("go2"), SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(new GameObject("somethingelse"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("go");

            yield return null;

            var items = SearchView.itemsSource.Cast<EntityHierarchyItem>().Select(x => x.CachedName);
            Assert.That(items, Is.EquivalentTo(new[] { SubSceneName, "go", "go2" }));
        }

        [UnityTest]
        public IEnumerator Search_QuerySearch()
        {
            // Adding a GameObject to a SubScene
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search($"c:{nameof(WorldTime)}");

            yield return null;

            var items = SearchView.itemsSource.Cast<EntityHierarchyItem>().Select(x => x.CachedName);
            Assert.That(items, Is.EquivalentTo(new[] { nameof(WorldTime) }));
        }

        [UnityTest]
        public IEnumerator Search_QueryAndNameSearch()
        {
            // Adding a GameObject to a SubScene
            SceneManager.MoveGameObjectToScene(new GameObject("abc"), SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(new GameObject("def"), SubScene.EditingScene);
            SceneManager.MoveGameObjectToScene(new GameObject("ghi"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search($"c:{nameof(EntityGuid)} abc");

            yield return null;

            var items = SearchView.itemsSource.Cast<EntityHierarchyItem>().Select(x => x.CachedName);
            Assert.That(items, Is.EquivalentTo(new[] { SubSceneName, "abc" }));
        }

        [UnityTest]
        public IEnumerator Search_NameSearch_NoResult()
        {
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("hello");

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));

            var centeredMessageElement = WindowRoot.Q<CenteredMessageElement>();
            Assert.That(centeredMessageElement, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(centeredMessageElement.Title, Is.EqualTo(EntityHierarchy.NoEntitiesFoundTitle));
            Assert.That(centeredMessageElement.Message, Is.Empty);
        }

        [UnityTest]
        public IEnumerator Search_NameSearch_NoResultThenMatchANewlyCreatedEntity()
        {
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("hello");

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            var centeredMessageElement = WindowRoot.Q<CenteredMessageElement>();
            Assert.That(centeredMessageElement, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));

            SceneManager.MoveGameObjectToScene(new GameObject("helloworld"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(centeredMessageElement, UIToolkitTestHelper.Is.Display(DisplayStyle.None));
        }

        [UnityTest]
        public IEnumerator Search_QuerySearch_NoResult()
        {
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            SearchElement.Search("c:TypeThatDoesntExist");

            yield return null;

            Assert.That(FullView, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(SearchView, UIToolkitTestHelper.Is.Display(DisplayStyle.None));

            var centeredMessageElement = WindowRoot.Q<CenteredMessageElement>();
            Assert.That(centeredMessageElement, UIToolkitTestHelper.Is.Display(DisplayStyle.Flex));
            Assert.That(centeredMessageElement.Title, Is.EqualTo(EntityHierarchy.ComponentTypeNotFoundTitle));
            Assert.That(centeredMessageElement.Message, Is.EqualTo(string.Format(EntityHierarchy.ComponentTypeNotFoundContent, "TypeThatDoesntExist")));
        }

        [UnityTest]
        public IEnumerator Search_QuerySearch_IncludeSpecialEntity([Values(typeof(Prefab), typeof(Disabled))] Type componentType)
        {
            SceneManager.MoveGameObjectToScene(new GameObject("go"), SubScene.EditingScene);

            yield return UpdateLiveLink();

            var e = EntityManager.CreateEntity();
            EntityManager.SetName(e, "Test entity");
            EntityManager.AddComponent<EntityGuid>(e);
            EntityManager.AddComponent(e, componentType);

            yield return SkipAnEditorFrameAndDiffingUtility();

            var expectedNode = EntityHierarchyNodeId.FromEntity(e);
            Assert.That(FullView.items.Cast<EntityHierarchyItem>().Select(i => i.NodeId), Does.Contain(expectedNode));

            SearchElement.Search($"c:{typeof(EntityGuid).FullName}");

            yield return SkipAnEditorFrameAndDiffingUtility();

            Assert.That(SearchView.itemsSource.Cast<EntityHierarchyItem>().Select(i => i.NodeId), Does.Contain(expectedNode));
        }

        [UnityTest]
        public IEnumerator Selection_DestroyingSelectedEntityDeselectInView()
        {
            var internalListView = FullView.Q<ListView>();

            var e = EntityManager.CreateEntity();
            var node = EntityHierarchyNodeId.FromEntity(e);

            yield return SkipAnEditorFrameAndDiffingUtility();

            Assert.That(internalListView.currentSelectionIds, Is.Empty);

            EntitySelectionProxy.SelectEntity(EntityManager.World, e);
            yield return null;

            Assert.That(internalListView.currentSelectionIds, Is.EquivalentTo(new[] { node.GetHashCode() }));

            EntityManager.DestroyEntity(e);
            yield return SkipAnEditorFrameAndDiffingUtility();

            Assert.That(internalListView.selectedItems, Is.Empty);
        }

        [UnityTest]
        public IEnumerator LoadDynamicSubScene_ShowsNewNodeInTreeView()
        {
            var subSceneName = "DynamicSubScene";
            var targetGO = new GameObject(subSceneName);
            var subsceneArgs = new SubSceneContextMenu.NewSubSceneArgs(targetGO, Scene, SubSceneContextMenu.NewSubSceneMode.EmptyScene);
            var subScene = SubSceneContextMenu.CreateNewSubScene(subSceneName, subsceneArgs, InteractionMode.AutomatedAction);
            SceneManager.MoveGameObjectToScene(new GameObject("go"), subScene.EditingScene);

            Object.DestroyImmediate(targetGO);
            Object.DestroyImmediate(subScene.gameObject);

            yield return UpdateLiveLink();

            Assert.That(FullView.items.Cast<EntityHierarchyItem>().Select(x => x.NodeId.Kind), Does.Not.Contains(NodeKind.DynamicSubScene));

            var sceneSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<SceneSystem>();
            var hash = sceneSystem.GetSceneGUID(Path.ChangeExtension(Path.Combine(TestAssetsDirectory, Scene.name, subSceneName), k_SceneExtension));
            sceneSystem.LoadSceneAsync(hash,
               new SceneSystem.LoadParameters
               {
                   Flags = SceneLoadFlags.NewInstance | SceneLoadFlags.BlockOnStreamIn | SceneLoadFlags.BlockOnImport
               });

            // Forcing update for SceneSystem to run even in edit mode
            World.DefaultGameObjectInjectionWorld.Update();
            yield return UpdateLiveLink();

            var dynamicSubSceneNode = FullView.items.Cast<EntityHierarchyItem>().SingleOrDefault(x => x.NodeId.Kind == NodeKind.DynamicSubScene);
            Assert.That(dynamicSubSceneNode, Is.Not.Null);
            Assert.That(dynamicSubSceneNode.CachedName, Is.EqualTo(subSceneName));
            Assert.That(dynamicSubSceneNode.Children, Is.Not.Empty);
        }

        // Creates the basic hierarchy for a single scene with a single subscene.
        static (TestHierarchy.TestNode root, TestHierarchy.TestNode subScene, int nextSceneId, int nextEntityId) CreateBaseHierarchyForSubscene()
        {
            var entityId = 0;
            var sceneId = 0;

            var rootNode = TestHierarchy.CreateRoot();

            rootNode.AddChildren(
                new EntityHierarchyNodeId(NodeKind.Entity, entityId++, 0),                                  // World Time Entity
                new EntityHierarchyNodeId(NodeKind.Entity, entityId++, 0),                                  // SubScene Entity
                new EntityHierarchyNodeId(NodeKind.Entity, entityId++, 0));                                 // SceneSection Entity

            var subSceneNode =
                rootNode.AddChild(EntityHierarchyNodeId.FromScene(sceneId++))                  // Main Scene
                                        .AddChild(EntityHierarchyNodeId.FromSubScene(sceneId++)); // SubScene

            return (rootNode, subSceneNode, sceneId, entityId);
        }
    }
}
