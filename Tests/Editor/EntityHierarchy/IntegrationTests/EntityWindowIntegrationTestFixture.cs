using System.Collections;
using NUnit.Framework;
using Unity.Editor.Bridge;
using Unity.Properties.UI;
using Unity.Scenes;
using Unity.Scenes.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.LowLevel;
using UnityEngine.UIElements;
using ListView = Unity.Editor.Bridge.ListView;

namespace Unity.Entities.Editor.Tests
{
    abstract class EntityWindowIntegrationTestFixture : BaseTestFixture
    {
        const string k_WorldName = "Test World";

        protected EntityManager EntityManager { get; private set; }
        protected EntityHierarchyWindow Window { get; private set; }
        protected TestHierarchyHelper AssertHelper { get; private set; }

        protected VisualElement WindowRoot => Window.rootVisualElement;
        protected SearchElement SearchElement => WindowRoot.Q<SearchElement>();
        protected TreeView FullView => WindowRoot.Q<TreeView>(Constants.EntityHierarchy.FullViewName);
        protected ListView SearchView => WindowRoot.Q<ListView>(Constants.EntityHierarchy.SearchViewName);

        World m_PreviousWorld;
        PlayerLoopSystem m_PreviousPlayerLoop;

        protected IEnumerator UpdateLiveLink()
        {
            LiveLinkConnection.GlobalDirtyLiveLink();
            yield return SkipAnEditorFrameAndDiffingUtility();
        }

        protected IEnumerator SkipAnEditorFrameAndDiffingUtility()
        {
            yield return SkipAnEditorFrame();
            Window.Update();
        }

        protected static IEnumerator SkipAnEditorFrame()
        {
            EditorUpdateUtility.EditModeQueuePlayerLoopUpdate();

            // Yield twice to ensure EditorApplication.update was invoked before resuming.
            yield return null;
            yield return null;
        }

        [SetUp]
        public virtual void Setup()
        {
            m_PreviousPlayerLoop = PlayerLoop.GetCurrentPlayerLoop();

            m_PreviousWorld = World.DefaultGameObjectInjectionWorld;
            DefaultWorldInitialization.Initialize(k_WorldName, true);
            EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            Window = CreateWindow();
            AssertHelper = new TestHierarchyHelper(Window.State);

            World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SceneSystem>().LoadSceneAsync(SubScene.SceneGUID, new SceneSystem.LoadParameters
            {
                Flags = SceneLoadFlags.BlockOnImport | SceneLoadFlags.BlockOnStreamIn
            });

            World.DefaultGameObjectInjectionWorld.Update();
        }

        [TearDown]
        public void TearDown()
        {
            DestroyWindow(Window);
            AssertHelper = null;

            World.DefaultGameObjectInjectionWorld.Dispose();
            World.DefaultGameObjectInjectionWorld = m_PreviousWorld;
            m_PreviousWorld = null;
            EntityManager = default;

            PlayerLoop.SetPlayerLoop(m_PreviousPlayerLoop);

            TearDownSubScene();
        }

        protected void TearDownSubScene()
        {
            foreach (var rootGO in SubScene.EditingScene.GetRootGameObjects())
                Object.DestroyImmediate(rootGO);
        }

        static EntityHierarchyWindow CreateWindow()
        {
            var window = EditorWindow.GetWindow<EntityHierarchyWindow>();
            window.DisableDifferCooldownPeriod = true;
            window.Update();
            window.ChangeCurrentWorld(World.DefaultGameObjectInjectionWorld);

            return window;
        }

        static void DestroyWindow(EditorWindow window)
        {
            window.Close();
            Object.DestroyImmediate(window);
        }
    }
}
