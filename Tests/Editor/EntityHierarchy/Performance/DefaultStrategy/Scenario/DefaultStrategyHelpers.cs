using Unity.Profiling;
using Unity.Transforms;

namespace Unity.Entities.Editor.Tests
{
    static class DefaultStrategyHelpers
    {
        static readonly ProfilerMarker k_UpdateParentingMarker = new ProfilerMarker($"{nameof(DefaultStrategyHelpers)}.{nameof(UpdateParenting)}");

        public static void UpdateParenting(World world)
        {
            using (k_UpdateParentingMarker.Auto())
            {
                var parentSystem = world.GetOrCreateSystem<EndFrameParentSystem>();
                parentSystem.Update();
                parentSystem.CompleteDependencyInternal();
            }
        }
    }
}
