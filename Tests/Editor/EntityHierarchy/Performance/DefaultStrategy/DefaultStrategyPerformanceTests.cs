using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime;
using JetBrains.Annotations;
using NUnit.Framework;
using Unity.PerformanceTesting;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.TestTools;

namespace Unity.Entities.Editor.Tests
{
    [TestFixture]
    [Category(Categories.Performance)]
    class DefaultStrategyPerformanceTests : EntityWindowIntegrationTestFixture
    {
        WorldGenerator m_CurrentGenerator;
        World m_CurrentWorld;
        EntityHierarchyScenario m_CurrentScenario;

        [UnitySetUp, UsedImplicitly]
        public IEnumerator SetUpGC()
        {
            GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);

            yield return SkipAnEditorFrame();
        }

        [UnityTest, Performance]
        public IEnumerator BasicScenarioRunner(
            [Values(ScenarioId.ScenarioA, ScenarioId.ScenarioB, ScenarioId.ScenarioC)]
            ScenarioId scenarioId,
            [Values(ItemsVisibility.AllExpanded, ItemsVisibility.AllCollapsed)]
            ItemsVisibility itemsVisibility)
        {
            const int warmupCount = 2;
            const int measurementsCount = 10;

            var profilerEnabled = Profiler.enabled;

            SetupProfilerReport($"scenario-{scenarioId.ToString()}-{itemsVisibility.ToString()}__init");
            StartProfiling();

            m_CurrentScenario = EntityHierarchyScenario.GetScenario(scenarioId, itemsVisibility);
            m_CurrentGenerator = new DefaultStrategyWorldGenerator(m_CurrentScenario);

            // Run Warmup
            for (var i = 0; i < warmupCount; ++i)
            {
                yield return SetUpBeforeMeasurement();
                MeasureMethod();
                yield return CleanUpAfterMeasurement();
            }

            StopProfiling();

            SetupProfilerReport($"scenario-{scenarioId.ToString()}-{itemsVisibility.ToString()}");

            // Run Measurements
            var groupName = m_CurrentScenario.ToString();
            for (var i = 0; i < measurementsCount; ++i)
            {
                yield return SetUpBeforeMeasurement();

                StartProfiling();
                using (Measure.Scope(groupName))
                {
                    MeasureMethod();
                }
                StopProfiling();

                yield return CleanUpAfterMeasurement();
            }

            m_CurrentGenerator.Dispose();

            Profiler.enabled = profilerEnabled;
        }

        void MeasureMethod()
        {
            Window.Update();
            Window.Repaint();
        }

        IEnumerator SetUpBeforeMeasurement()
        {
            yield return SkipAnEditorFrame();

            m_CurrentWorld = m_CurrentGenerator.Get();

            yield return PrepareWindow();

            DefaultStrategyChangeFunctions.PerformParentsShuffle(m_CurrentWorld, m_CurrentScenario);
        }

        IEnumerator CleanUpAfterMeasurement()
        {
            yield return SkipAnEditorFrame();

            m_CurrentWorld.Dispose();
            m_CurrentWorld = null;
            Window.ChangeCurrentWorld(null);

            yield return SkipAnEditorFrameAndDiffingUtility();

            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);

            yield return SkipAnEditorFrame();
        }

        IEnumerator PrepareWindow()
        {
            Window.ChangeCurrentWorld(m_CurrentWorld);

            // Currently, only fully expanded and fully collapsed are supported.
            // More granular visibility may be implemented, if needed.
            if (m_CurrentScenario.PercentageOfVisibleItems == 0.0f)
                FullView.FastCollapseAll();
            else
                FullView.FastExpandAll();

            yield return SkipAnEditorFrameAndDiffingUtility();
            yield return SkipAnEditorFrame();
        }

        [Conditional("SAVE_PERFORMANCE_TESTS_PROFILING_LOGS")]
        static void SetupProfilerReport(string logName)
        {
            var reportDirectory = Path.Combine(Application.dataPath, "../Library/PerformanceReports/");
            Directory.CreateDirectory(reportDirectory);

            Profiler.logFile = Path.GetFullPath(Path.Combine(reportDirectory, logName));
            Profiler.enableBinaryLog = true;

            // Allow 1GB instead of default 512MB
            Profiler.maxUsedMemory = 1024 * 1024 * 1024;

            Debug.Log($"Prepared profiler log file: {Profiler.logFile}");
        }

        [Conditional("SAVE_PERFORMANCE_TESTS_PROFILING_LOGS")]
        static void StartProfiling()
            => Profiler.enabled = true;

        [Conditional("SAVE_PERFORMANCE_TESTS_PROFILING_LOGS")]
        static void StopProfiling()
            => Profiler.enabled = false;
    }
}
