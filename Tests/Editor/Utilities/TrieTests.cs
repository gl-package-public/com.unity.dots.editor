using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.PerformanceTesting;

namespace Unity.Entities.Editor.Tests
{
    class TrieTests
    {
        [Test]
        public void Trie_IndexAndSearch()
        {
            var trie = new Trie();
            trie.Index("bonjour");
            trie.Index("bon");
            trie.Index("bonsoir");

            Assert.That(trie.Search("bon").ToArray(), Is.EquivalentTo(new[] { "bonjour", "bon", "bonsoir" }));
            Assert.That(trie.Search("bonjour").ToArray(), Is.EquivalentTo(new[] { "bonjour" }));
            Assert.That(trie.Search("hello").ToArray(), Is.Empty);
        }

        [Test]
        public void Trie_SearchOnEmpty()
        {
            var trie = new Trie();
            Assert.That(trie.Search("something"), Is.Empty);
        }

        [Test]
        public void Trie_IndexDuplicate()
        {
            var trie = new Trie<int>();
            trie.Index("bonjour", 10);
            trie.Index("bonjour", 20);
            trie.Index("bonjour", 20);
            trie.Index("bonsoir", 30);

            Assert.That(trie.Search("bon"), Is.EquivalentTo(new[] { 10, 20, 30 }));
        }

        [Test]
        public void Trie_KeyCaseInsensitive()
        {
            var trie = new Trie<int>();
            trie.Index("aBc", 10);
            trie.Index("abc", 10);
            trie.Index("abC", 10);

            {
                var expected = new[] { 10 };
                Assert.That(trie.Search("a"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("aBc"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("abC"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("abc"), Is.EquivalentTo(expected));
            }

            trie.Index("aBc", 20);
            trie.Index("abc", 30);
            trie.Index("abC", 40);

            {
                var expected = new[] { 10, 20, 30, 40 };
                Assert.That(trie.Search("a"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("aBc"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("abC"), Is.EquivalentTo(expected));
                Assert.That(trie.Search("abc"), Is.EquivalentTo(expected));
            }
        }

        [Test]
        public void Trie_IndexBetweenSearch()
        {
            var trie = new Trie<int>();
            trie.Index("bonjour", 10);

            Assert.That(trie.Search("bon"), Is.EquivalentTo(new[] { 10 }));

            trie.Index("bonsoir", 20);

            Assert.That(trie.Search("bon"), Is.EquivalentTo(new[] { 10, 20 }));
        }

        [Test, Performance]
        public void Trie_IndexTypesPerfTests()
        {
            var listCache = TypeManager.GetAllTypes().Where(t => t.Type != null).Select(t => t.Type.Name.ToLowerInvariant()).Distinct().ToList();
            var trie = new Trie(TypeManager.GetAllTypes().Where(t => t.Type != null).Select(t => t.Type.Name));

            Measure.Method(() =>
            {
                TypeManager.GetAllTypes().Where(t => t.Type != null).Select(t => t.Type.Name.ToLowerInvariant()).Distinct().ToList();
            })
            .SampleGroup("List indexing")
            .WarmupCount(10)
            .MeasurementCount(100)
            .Run();

            Measure.Method(() =>
            {
                var t = new Trie();
                t.Index(TypeManager.GetAllTypes().Where(info => info.Type != null).Select(info => info.Type.Name));
            })
            .SampleGroup("Trie indexing")
            .WarmupCount(10)
            .MeasurementCount(100)
            .Run();

            Measure.Method(() =>
            {
                var items = listCache.Where(x => x.StartsWith("ro")).ToArray();
            })
            .SampleGroup("Search in List index")
            .WarmupCount(10)
            .MeasurementCount(100)
            .Run();

            Measure.Method(() =>
            {
                var items = trie.Search("ro").ToArray();
            })
            .SampleGroup("Search in Trie index")
            .WarmupCount(10)
            .MeasurementCount(100)
            .Run();
        }

        [Test, Performance]
        public void Trie_ScaleTest()
        {
            var allTypes = new HashSet<string>();
            var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in allAssemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    allTypes.Add(type.Name.ToLowerInvariant());
                }
            }

            var step = allTypes.Count / 5;
            for (var length = step; length < allTypes.Count; length += step)
            {
                Measure.Method(() =>
                {
                    var t = new Trie(allTypes.Take(length));
                })
                .SampleGroup($"Indexing {length} / {allTypes.Count}")
                .WarmupCount(1)
                .MeasurementCount(5)
                .Run();
            }
        }

        [Test]
        public void Trie_IndexAllTypesAndExportStatistics()
        {
            var allTypes = TypeManager.GetAllTypes().Where(t => t.Type != null).Select(t => t.Type.Name).ToArray();
            var allTypesCount = allTypes.Distinct().Count();

            var trie = new Trie(allTypes);

            var (totalNodeCount, nodeCountHavingValue, subNodesPerNode) = trie.GetStatistics();

            Assert.That(nodeCountHavingValue, Is.EqualTo(allTypesCount));

            var maxSubNodeCount = subNodesPerNode.Max();
            Debug.Log($"Total of {totalNodeCount} nodes, {nodeCountHavingValue} nodes with a value attached, avg of {subNodesPerNode.Average()} children per node, min: {subNodesPerNode.Min()}, max: {maxSubNodeCount}");

            for (var i = 0; i <= maxSubNodeCount; i++)
            {
                Debug.Log($"{i} children: {subNodesPerNode.Count(x => x == i)}");
            }
        }
    }
}
