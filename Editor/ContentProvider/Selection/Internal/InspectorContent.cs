using Unity.Properties.UI.Internal;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Unity.Properties.UI
{
    sealed class InspectorContent : ScriptableObject
    {
        public static InspectorContent Show(ContentProvider provider)
        {
            var dynamicContent = CreateInstance<InspectorContent>();
            dynamicContent.SetContent(new SerializableContent {Provider = provider});
            Selection.activeObject = dynamicContent;
            return dynamicContent;
        }
        
        [SerializeField] SerializableContent m_Content;
        public SerializableContent Content => m_Content;
        public BindableElement Root { get; private set; }

        void SetContent(SerializableContent content)
        {
            m_Content = content;
            m_Content.Root = Root;
            m_Content.Initialize();
            m_Content.Load();
        }
        
        // Invoked by the Unity update loop
        void OnEnable() 
        {
            Root = new BindableElement();
            if (null != m_Content)
                SetContent(m_Content);
        }
    }
}
