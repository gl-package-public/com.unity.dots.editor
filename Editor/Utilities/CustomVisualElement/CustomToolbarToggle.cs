using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Image = UnityEngine.UIElements.Image;

namespace Unity.Entities.Editor
{
    class CustomToolbarToggle : ToolbarToggle
    {
        /// <summary>
        /// <para>Constructor.</para>
        /// </summary>
        public CustomToolbarToggle()
        {
            Resources.Templates.DotsEditorCommon.AddStyles(this);

            this.text = "temp";
            var l = this.Q<Label>();
            l.AddToClassList(UssClasses.DotsEditorCommon.CustomToolbarToggleOnlyLabel);

            this.AddToClassList(UssClasses.DotsEditorCommon.CustomToolbarToggle);
            l.parent.AddToClassList(UssClasses.DotsEditorCommon.CustomToolbarToggleLabelParent);
        }

        public void AddPreIcon(Image preIcon)
        {
            this.Insert(0, preIcon);
            preIcon.RegisterCallback<MouseDownEvent>(evt =>
            {
                this.value = !this.value;
            });
            preIcon.AddToClassList(UssClasses.DotsEditorCommon.CustomToolbarToggleIcon);

            var l = this.Q<Label>();

            l.RemoveFromClassList(UssClasses.DotsEditorCommon.CustomToolbarToggleOnlyLabel);
            l.AddToClassList(UssClasses.DotsEditorCommon.CustomToolbarToggleLabel);
        }
    }
}
