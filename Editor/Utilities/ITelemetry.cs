﻿namespace Unity.Entities.Editor
{
    interface ITelemetry
    {
        string Name { get; }
    }
}
