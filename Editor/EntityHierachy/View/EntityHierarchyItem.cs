using System;
using System.Collections.Generic;
using Unity.Editor.Bridge;
using Unity.Profiling;
using UnityEditor;

namespace Unity.Entities.Editor
{
    class EntityHierarchyItem : ITreeViewItem
    {
        static readonly string k_ChildrenListModificationExceptionMessage = L10n.Tr($"{nameof(EntityHierarchyItem)} does not allow external modifications to its list of children.");

        static readonly ProfilerMarker k_AcquireMarker = new ProfilerMarker($"{nameof(EntityHierarchyItem)}.{nameof(Acquire)}()");
        static readonly ProfilerMarker k_ReleaseMarker = new ProfilerMarker($"{nameof(EntityHierarchyItem)}.{nameof(Release)}()");
        static readonly ProfilerMarker k_PopulateChildrenMarker = new ProfilerMarker($"{nameof(EntityHierarchyItem)}.{nameof(PopulateChildren)}()");

        internal static readonly BasicPool<EntityHierarchyItem> Pool = new BasicPool<EntityHierarchyItem>(() => new EntityHierarchyItem());

        readonly List<EntityHierarchyItem> m_Children = new List<EntityHierarchyItem>();
        bool m_ChildrenInitialized;

        // Caching name and pre-lowercased name to speed-up search
        string m_CachedName;

        // Public for binding with SearchElement
        // ReSharper disable once InconsistentNaming
        public string m_CachedLowerCaseName;

        IEntityHierarchy m_EntityHierarchy;

        EntityHierarchyItem() { }

        public static EntityHierarchyItem Acquire(ITreeViewItem parentItem, in EntityHierarchyNodeId nodeId, IEntityHierarchy entityHierarchy)
        {
            using (k_AcquireMarker.Auto())
            {
                var item = Pool.Acquire();
                item.parent = parentItem;
                item.NodeId = nodeId;
                item.id = nodeId.GetHashCode();
                item.m_EntityHierarchy = entityHierarchy;

                return item;
            }
        }

        public EntityHierarchyNodeId NodeId { get; private set; }

        public IEntityHierarchyState HierarchyState => m_EntityHierarchy.State;

        public World World => m_EntityHierarchy.World;

        public List<EntityHierarchyItem> Children
        {
            get
            {
                if (!m_ChildrenInitialized)
                {
                    PopulateChildren();
                    m_ChildrenInitialized = true;
                }
                return m_Children;
            }
        }

        public string CachedName => m_CachedName ?? (m_CachedName = HierarchyState.GetNodeName(NodeId));

        public void PrepareSearcheableName()
        {
            if (m_CachedLowerCaseName == null)
                m_CachedLowerCaseName = CachedName.ToLowerInvariant();
        }

        public int id { get; private set; }

        public ITreeViewItem parent { get; private set; }

        IEnumerable<ITreeViewItem> ITreeViewItem.children => Children;

        public bool hasChildren => HierarchyState.HasChildren(NodeId);

        void ITreeViewItem.AddChild(ITreeViewItem _) => throw new NotSupportedException(k_ChildrenListModificationExceptionMessage);

        void ITreeViewItem.AddChildren(IList<ITreeViewItem> _) => throw new NotSupportedException(k_ChildrenListModificationExceptionMessage);

        void ITreeViewItem.RemoveChild(ITreeViewItem _) => throw new NotSupportedException(k_ChildrenListModificationExceptionMessage);

        public void Release()
        {
            using (k_ReleaseMarker.Auto())
            {
                NodeId = default;
                m_CachedName = null;
                m_CachedLowerCaseName = null;
                m_EntityHierarchy = null;
                parent = null;
                m_ChildrenInitialized = false;

                foreach (var child in m_Children)
                {
                    child.Release();
                }

                m_Children.Clear();
                Pool.Release(this);
            }
        }

        void PopulateChildren()
        {
            using (k_PopulateChildrenMarker.Auto())
            {
                using (var childrenNodes = PooledList<EntityHierarchyNodeId>.Make())
                {
                    HierarchyState.GetChildren(NodeId, childrenNodes.List);

                    foreach (var node in childrenNodes.List)
                    {
                        m_Children.Add(Acquire(this, node, m_EntityHierarchy));
                    }
                }
            }
        }
    }
}
